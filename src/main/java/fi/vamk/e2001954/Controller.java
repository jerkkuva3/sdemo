package fi.vamk.e2001954;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/")
    public String swagger() {
        return "<script>window.location.replace(\"/swagger-ui.html\")</script>";
    }  
    @RequestMapping("/test")
    public String test(){
        return "{\"id\":1}";
    }
}
